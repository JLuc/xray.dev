<?php

define ('XRAY_PATTERN_SESSION', '/_([a-f0-9]{8}|)$/i');
define ('XRAY_PATTERN_SESSION_AUTH', '/_([a-f0-9]{8})$/i');
define ('XRAY_PATTERN_SESSION_ANON', '/_$/i');
define ('XRAY_PATTERN_NON_SESSION', '/[^_](?<!_[a-f0-9]{8})$/i');
define ('XRAY_PATTERN_TALON', '/^(.*)(?<!_[a-f0-9]{8})(?<!_)(_([a-f0-9]{8})?|)$/i');

if (!function_exists('cache_est_sessionne')) {
	function cache_est_sessionne ($nomcache) {
		if (preg_match (XRAY_PATTERN_SESSION_AUTH, $nomcache))
			return 'session_auth';
		elseif (preg_match (XRAY_PATTERN_SESSION_ANON, $nomcache))
			return 'session_anon';
		else
			return false;
	}

	function cache_est_talon ($nomcache,&$data='') {
		if (preg_match(XRAY_PATTERN_SESSION, $nomcache))
			return false;
		if (!is_array($data)) // textwheels par exemple
			return false;
		return !isset($data['contexte']);
	}
}

//
// Ça enlève le préfixe non affiché plus le préfixe md5
// et ça supprime le suffixe de session
// MAIS ça ne supprime pas les suffixes /12354 ou /spip ou autres
// qu'il y a pour les squelettes appelés en tant que pages
// et qui viennent de la forme de l'url visible par l'internaute
//
function cache_get_squelette($cle) {
	$squelette = substr(str_replace(XRAY_NEPASAFFICHER_DEBUTNOMCACHE, '', $cle), 33);
	$squelette = preg_replace(XRAY_PATTERN_SESSION, '', $squelette);
	return $squelette;
}

////////////////////////////////////////////////////////////////////////

// copie un peu modifiée de la fonction définie dans public/cacher.php
if (!function_exists('gunzip_page')) {
	function gunzip_page(&$page) {
		if (isset ($page['gz']) and $page['gz']) {
			$page['texte'] = gzuncompress($page['texte']);
			$page['gz'] = false; // ne pas gzuncompress deux fois une meme page
		}
	}
}
else
	die("La fonction gunzip_page ne devrait pas être déjà définie"); // à défaut de disposer de la lib nobug

// Strings utils

function ajuste_longueur_html($str) {
	$court = (!isset($_GET['ZOOM']) or ($_GET['ZOOM'] != 'TEXTELONG'));
	$str = trim(preg_replace("/^\s*$/m", '', $str)); // enlève lignes vides... mais il en reste qqunes
	if ($court and (mb_strlen($str) > MAXLEN_HTMLCOURT))
		$str = mb_substr($str, 0, MAXLEN_HTMLCOURT) . '...';
	elseif (!$str)
		$str = '(vide)';
	return $str;
}

function is_serialized($str) {
	return ($str == serialize(false) || @unserialize($str) !== false);
}

function get_serial_class($serial) {
	$types = array(
		's' => 'string',
		'a' => 'array',
		'b' => 'bool',
		'i' => 'int',
		'd' => 'float',
		'N;' => 'NULL'
	);

	$parts = explode(':', $serial, 4);
	return isset($types[$parts[0]]) ? $types[$parts[0]] : trim($parts[2], '"');
}



function joli_contexte($contexte) {
	global $MY_SELF;
	$return = '';
	if (!$contexte)
		return '';
	if (!is_array($contexte))
		return $contexte;
	foreach ($contexte as $var => $val) {
		$print = print_r($val, 1);
		if (!is_array($val) and (!$val or (strpos("\n", $val) === false))) {
			$ligne = "[$var] => $val";
			if (strlen($val) < 100) {
				$url = parametre_url (parametre_url($MY_SELF,'WHERE', 'CONTEXTE'),
					'SEARCH', "\\[$var\\] => $val$");
				$title = "Voir tous les caches ayant cette même valeur de contexte";
				$return .= "<a href='$url' title='$title'><xmp>[$var] => $val</xmp></a>";
				if (substr($var,0,3)== 'id_')
					$return .= bouton_objet(substr($var,3), $val, $contexte);
			}
			else
				$return .= "<xmp>$ligne</xmp>";
			$return .= "<br>";
		}
		else
			$return .= "<xmp>[$var] => (".gettype($val).") $print</xmp>";
	};
	return $return;
}

function joli_cache($extra) {
	if (is_array($extra)
		and isset($extra['texte']))
		$extra['texte'] = ajuste_longueur_html($extra['texte']);
	// sinon c'est pas un squelette spip, par exemple une textwheel
	// ou juste un talon ou juste une des métadonnées du cache

	$print=print_r($extra,1);

	if (!is_array($extra))
		return "<xmp>".ajuste_longueur_html($print)."</xmp>";

	// On enlève 'Array( ' au début et ')' à la fin
	$print = trim(substr($print, 5), " (\n\r\t");
	$print = substr ($print, 0, -1);

	// rien à améliorer s'il n'y a ni la source ni le squelette
	if (!isset($extra['source']) and !isset($extra['squelette']))
		return "<xmp>$print</xmp>";

	// [squelette] => html_5731a2e40776724746309c16569cac40
	// et [source] => plugins/paeco/squelettes/inclure/element/tag-rubrique.html
	$print = preg_replace_callback("/\[(squelette|source)\]\s*=>\s*(html_[a-f0-9]{32}+|[\w_\.\/\-]+\.html)$/im",
		function($match)
		{
			if (!defined('_SPIP_ECRIRE_SCRIPT'))
				spip_initialisation_suite();	// pour define(_DIR_CACHE)

			switch ($match[1]) {
				case 'squelette' : // cache squelette intermédiaire, en php
					$source = trim(_DIR_CACHE, '/').'/skel/'.$match[2].'.php';
					$title = "Squelette compilé : cache intermédiaire en php";
					break;
				case 'source' :
					$source = '../'.$match[2];
					$title = "Source du squelette SPIP, avec boucles, balises etc";
					break;
			}
			return "[{$match[1]}] => </xmp><a title='{$title}' 
						href='".generer_url_ecrire('xray', "SOURCE=$source")."' 
						target='blank'><xmp>{$match[2]}</xmp> <small>&#128279;</small></a><xmp>";
		},
		$print);
	$print = preg_replace('/^    /m', '', $print);
	return "<xmp>$print</xmp>";;
}

function bouton_session($id_session, $url_session) {
	if (function_exists('cachelab_cibler')) {
		$title = cachelab_cibler('get_html', array('chemin'=>'xray_marqueur_visible_'.$id_session))."\n";
	}
	else
		$title = 'Installez CacheLab pour bénéficier d’informations sur cette session.';
	$title = preg_replace("/\n+/", "\n", $title);
	$title .= "\nVoir tous les caches sessionnés de cet internaute";
	return "<a href=\"$url_session\" title=\"$title\">[session]</a>";
}

function bouton_objet($objet, $id_objet, $contexte) {
	$objet_visible = $objet;
	if ($objet == 'secteur')
		$objet = 'rubrique';
	elseif (($objet == 'objet')	and isset ($contexte['objet']))
	{
		$objet_visible = $objet = $contexte['objet'];
	};
	global $MY_SELF;
	return "<a href='/ecrire/?exec=$objet&id_$objet=$id_objet' target='blank' 
				style='float: right'
				title=\"" . attribut_html(generer_info_entite($id_objet, $objet, 'titre', 'etoile')) . "\">
				[voir $objet_visible]
			</a>
			";
}

if (!function_exists('plugin_est_actif')) {
	function plugin_est_actif($prefixe) {
		$f = chercher_filtre('info_plugin');
		return $f($prefixe, 'est_actif');
	}
}

function antislash ($str) {
	return str_replace('/', '\/', $str);
}


// "define if not defined"
function defaults($d, $v) {
	if (!defined($d))
		define($d, $v); // or just @define(...)
}

function xray_duration($ts)
{
	global $time;
	$years = (int) ((($time - $ts) / (7 * 86400)) / 52.177457);
	$rem   = (int) (($time - $ts) - ($years * 52.177457 * 7 * 86400));
	$weeks = (int) (($rem) / (7 * 86400));
	$days  = (int) (($rem) / 86400) - $weeks * 7;
	$hours = (int) (($rem) / 3600) - $days * 24 - $weeks * 7 * 24;
	$mins  = (int) (($rem) / 60) - $hours * 60 - $days * 24 * 60 - $weeks * 7 * 24 * 60;
	$str   = '';
	if ($years == 1)
		$str .= "$years year, ";
	if ($years > 1)
		$str .= "$years years, ";
	if ($weeks == 1)
		$str .= "$weeks week, ";
	if ($weeks > 1)
		$str .= "$weeks weeks, ";
	if ($days == 1)
		$str .= "$days day,";
	if ($days > 1)
		$str .= "$days days,";
	if ($hours == 1)
		$str .= " $hours hour and";
	if ($hours > 1)
		$str .= " $hours hours and";
	if ($mins == 1)
		$str .= " 1 minute";
	else
		$str .= " $mins minutes";
	return $str;
}


// pretty printer for byte values
//
function bsize($s)
{
	foreach (array(
		         '',
		         'K',
		         'M',
		         'G'
	         ) as $i => $k) {
		if ($s < 1024)
			break;
		$s /= 1024;
	}
	return sprintf("%5.1f %sBytes", $s, $k);
}

// sortable table header in "scripts for this host" view
function sortheader($key, $name, $extra = '')
{
	global $MYREQUEST;

	// fix apcu l'affichage des headers ne doit pas changer $MYREQUEST
	$sort = $MYREQUEST['SORT'];
	if (!$sort)
		$sort = 'D';
	if ($MYREQUEST['S_KEY'] == $key)
		$sort = (($sort == 'A') ? 'D' : 'A');

//	global $MY_SELF_WO_SORT; // fix apcu : il faut global ici aussi
//	$url = "$MY_SELF_WO_SORT$extra&S_KEY=$key&SORT=$SORT";
	global $MY_SELF;
	$url = parametre_url(parametre_url($MY_SELF.$extra,'S_KEY',$key),'SORT', $sort);
	return "<a class=sortable href='$url'>$name</a>";
}

// create menu entry
function menu_entry($ob, $title)
{
	global $MYREQUEST;
	global $MY_SELF; // fix apcu
	if ($MYREQUEST['OB'] != $ob) {
		return "<li><a href='" . parametre_url($MY_SELF, 'OB', $ob) . "'>$title</a></li>";
	} else if (empty($MYREQUEST['SH'])) {
		return "<li><span class=active>$title</span></li>";
	} else {
		return "<li><a class=\"child_active\" href='$MY_SELF'>$title</a></li>";
	}
}

function block_sort($array1, $array2)
{
	if ($array1['offset'] > $array2['offset']) {
		return 1;
	} else {
		return -1;
	}
}

