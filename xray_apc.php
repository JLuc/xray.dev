<?php

/*
+----------------------------------------------------------------------+
| APC                                                                  |
+----------------------------------------------------------------------+
| Copyright (c) 2006-2011 The PHP Group                                |
+----------------------------------------------------------------------+
| This source file is subject to version 3.01 of the PHP license,      |
| that is bundled with this package in the file LICENSE, and is        |
| available through the world-wide-web at the following url:           |
| http://www.php.net/license/3_01.txt                                  |
| If you did not receive a copy of the PHP license and are unable to   |
| obtain it through the world-wide-web, please send a note to          |
| license@php.net so we can mail you a copy immediately.               |
+----------------------------------------------------------------------+
| Authors du apcu.php d'origine :                                      |
|          Ralf Becker <beckerr@php.net>                               |
|          Rasmus Lerdorf <rasmus@php.net>                             |
|          Ilia Alshanetsky <ilia@prohost.org>                         |
| Auteur des adaptations et du plugin SPIP :                           |
|          JLuc http://contrib.spip.net/JLuc                           |
+----------------------------------------------------------------------+

All other licensing and usage conditions are those of the PHP Group.

*/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
include_spip ('inc/autoriser');
if (!autoriser('webmestre'))
	die("Autorisation non accordée : devenez webmestre d'abord.");
include_spip('inc/filtres');
include_spip('inc/cachelab');

$VERSION = '$Id$';

////////// READ OPTIONAL CONFIGURATION FILE ////////////
if (file_exists("apc.conf.php"))
	include("apc.conf.php");

include_spip ('inc/xray_utils');

////////// DEFAULT CONFIG AREA /////////////////////////

defaults('MAXLEN_HTMLCOURT', 1000);	// Couper les html

// (beckerr) I'm using a clear text password here, because I've no good idea how to let
//           users generate a md5 or crypt password in a easy way to fill it in above

// defaults('DATE_FORMAT', "d.m.Y H:i:s");	// German
defaults('DATE_FORMAT', 'Y/m/d H:i:s'); // US

defaults('GRAPH_SIZE', 400); // Image size

//defaults('PROXY', 'tcp://127.0.0.1:8080');

// _CACHE_NAMESPACE est défini par memoization et préfixe chaque nom de cache SPIP
// On ne souhaite pas que cette partie du nom s'affiche sur chaque ligne
define('XRAY_NEPASAFFICHER_DEBUTNOMCACHE', _CACHE_NAMESPACE.'cache:');

////////// END OF DEFAULT CONFIG AREA /////////////////////////////////////////////////////////////

include_spip ('inc/xray_apc');

// rewrite $PHP_SELF to block XSS attacks
//
$PHP_SELF = isset($_SERVER['PHP_SELF']) ? htmlentities(strip_tags($_SERVER['PHP_SELF'], ''), ENT_QUOTES, 'UTF-8') : '';

$time     = time();
$host     = php_uname('n');
if ($host) {
	$host = '(' . $host . ')';
}
if (isset($_SERVER['SERVER_ADDR'])) {
	$host .= ' (' . $_SERVER['SERVER_ADDR'] . ')';
}

// operation constants
define('OB_HOST_STATS', 1);
define('OB_USER_CACHE', 2);
define('OB_VERSION_CHECK', 3);
define('OB_CACHELAB', 4);

// check validity of input variables
$vardom = array(
	'exec' => '/^[a-zA-Z0-9_\-\.]+$/', // pour #URL_ECRIRE{xray}
	'OB' => '/^\d+$/', // operational mode switch
	'CC' => '/^[01]$/', // clear cache requested
	'PP' => '/^[01]$/', // Purger Précache de compilation des squelettes en plus de vider le cache APC user
	'DU' => '/^.*$/', // Delete User Key
	'SH' => '/^[a-z0-9]*$/', // shared object description
	
	'IMG' => '/^[123]$/', // image to generate
	'SOURCE' => '/^[a-zA-Z0-9_\-\.\/]+$/', // file source to display
//	'LO' => '/^1$/', // login requested
	'TYPELISTE' => '/^(caches|squelettes)$/',
	'COUNT' => '/^\d+$/', // number of line displayed in list
	'S_KEY' => '/^[AHSMCDTZ]$/', // first sort key
	'SORT' => '/^[DA]$/', // second sort key
	'AGGR' => '/^\d+$/', // aggregation by dir level
	'SEARCH' => '~.*~',
	'TYPECACHE' => '/^(|ALL|NON_SESSIONS|SESSIONS|SESSIONS_AUTH|SESSIONS_NONAUTH|SESSIONS_TALON|FORMULAIRES)$/', //
	'ZOOM' => '/^(|TEXTECOURT|TEXTELONG)$/', //
	'WHERE' => '/^(|ALL|HTML|META|CONTEXTE)$/', // recherche dans le contenu
	'EXTRA' => '/^(|CONTEXTE|CONTEXTES_SPECIAUX|HTML_COURT|INFO_AUTEUR|INFO_OBJET_SPECIAL|INVALIDEURS|INVALIDEURS_SPECIAUX|INCLUSIONS'
		.(plugin_est_actif('macrosession') ? '|MACROSESSIONS|MACROAUTORISER' : '')
		.')$/'		// Affichage pour chaque élément de la liste
);

global $MYREQUEST; // fix apcu
	$MYREQUEST = array();

// handle POST and GET requests
if (empty($_REQUEST)) {
	if (!empty($_GET) && !empty($_POST)) {
		$_REQUEST = array_merge($_GET, $_POST);
	} else if (!empty($_GET)) {
		$_REQUEST = $_GET;
	} else if (!empty($_POST)) {
		$_REQUEST = $_POST;
	} else {
		$_REQUEST = array();
	}
}

// check parameter syntax
foreach ($vardom as $var => $dom) {
	if (!isset($_REQUEST[$var]))
		$MYREQUEST[$var] = NULL;
	else if (!is_array($_REQUEST[$var]) && preg_match($dom . 'D', $_REQUEST[$var]))
		$MYREQUEST[$var] = $_REQUEST[$var];
	else {
		echo "<xmp>ERREUR avec parametre d'url « $var » qui vaut « {$_REQUEST[$var]} »</xmp>";
		$MYREQUEST[$var] = $_REQUEST[$var] = NULL;
	}
}

// check parameter semantics
if (empty($MYREQUEST['S_KEY']))
	$MYREQUEST['S_KEY'] = "H";
if (empty($MYREQUEST['SORT']))
	$MYREQUEST['SORT'] = "D";
if (empty($MYREQUEST['OB']))
	$MYREQUEST['OB'] = OB_HOST_STATS;
if (!isset($MYREQUEST['COUNT']))
	$MYREQUEST['COUNT'] = 20;
if (!isset($MYREQUEST['EXTRA']))
	$MYREQUEST['EXTRA'] = '';
if (!isset($MYREQUEST['ZOOM']))
	$MYREQUEST['ZOOM'] = 'TEXTECOURT';
if (!isset($MYREQUEST['TYPELISTE']))
	$MYREQUEST['TYPELISTE'] = 'caches';

global $MY_SELF; // fix apcu
global $MY_SELF_WO_SORT; // fix apcu
$MY_SELF_WO_SORT = "$PHP_SELF" . "?COUNT=" . $MYREQUEST['COUNT'] . "&SEARCH=" . $MYREQUEST['SEARCH'] . "&TYPECACHE=" . $MYREQUEST['TYPECACHE'] . "&ZOOM=" . $MYREQUEST['ZOOM'] . "&EXTRA=" . $MYREQUEST['EXTRA'] . "&WHERE=" . $MYREQUEST['WHERE'] . "&exec=" . $MYREQUEST['exec'] . "&OB=" . $MYREQUEST['OB']. "&TYPELISTE=" . $MYREQUEST['TYPELISTE'];
$MY_SELF = $MY_SELF_WO_SORT . "&S_KEY=" . $MYREQUEST['S_KEY'] . "&SORT=" . $MYREQUEST['SORT'];

$self_pour_lien = 
	"http" . (!empty($_SERVER['HTTPS']) ? "s" : "") . "://" 
	. $_SERVER['SERVER_NAME']
	// parametre_url fait un urlencode bienvenu pour les regexp qui peuvent contenir des ?
	. parametre_url($_SERVER['REQUEST_URI'], 'SEARCH', @$_REQUEST['SEARCH']);

global $IMG_BASE;
$IMG_BASE = "$PHP_SELF" . "?exec=" . $MYREQUEST['exec'];
// echo "IMG_BASE=$IMG_BASE<br>";

// clear APC cache
if (isset($MYREQUEST['CC']) && $MYREQUEST['CC']) {
	apcu_clear_cache();
}

// clear APC & SPIP cache
if (isset($MYREQUEST['PP']) && $MYREQUEST['PP']) {
	include_spip('inc/invalideur');
	purger_repertoire(_DIR_SKELS);
	apcu_clear_cache();
	ecrire_meta('cache_mark', time());
}

if (!empty($MYREQUEST['DU'])) {
	apcu_delete($MYREQUEST['DU']);
}

if (!function_exists('apcu_cache_info')) {
	echo "No cache info available.  APC does not appear to be running.";
	exit;
}

$cache = apcu_cache_info();

$mem = apcu_sma_info();

// don't cache this page
//
header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0


if (isset($MYREQUEST['SOURCE']) and $MYREQUEST['SOURCE']) {
	echo '<xmp>'.file_get_contents ($MYREQUEST['SOURCE']).'</xmp>';
	exit;
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head><title>XRay - APCu Infos sur les caches SPIP</title>
<link rel="stylesheet" href="<? echo find_in_path('inc/xray.css'); ?>">
</head>
<body>
<div class="head">
	<h1 class="apc">
		<div class="logo"><span class="logo"><a href="http://pecl.php.net/package/APCu">APCu</a></span></div>
		<div class="nameinfo" style="display: inline">
			User Cache
			<a href='https://contrib.spip.net/4946'>
			<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAAEwAAABMBDsgnAwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAARiSURBVFiFxVdNSBtbFP5m8pqpRmw2EhAU/6JtQAStG5VEiC5cCkLBhcQuXIi2i0ppSZ+laGmxghJFwU1cZCe6cFFXKlYENybSQqMENa1SGLUEQ6iZ5CXfW/S9YEwm0YLtgUuY852f7557z5mJIAjCmSiKIgABPyXplyTS6a9rp4YLABLInxDxtyQRRQwMDKCioiItzpteDoeDiqKwpKQkHY74TSavra0lSb569UrNJjMBm83Guro6VVyj0fDt27fUarVp8cnJSX779o2SJKXF/8p0dgCg0+mwtLSEhoYGaLVa1NXVQa/Xw+12w+Px4N27d+jo6MCLFy/S+peXl2NzcxOKoqjmyFiBwsJCqkksFiNJjo2Nqfp//vyZQ0NDqrgoCAIySWtrKwDgx48fF3sZABAOhwEA9+/fV/XPz89HQUGBKp61De/duwcAePjwISorK7G7uwtFUdDU1ASLxQIAKC0tVfX3+Xyq7QcAEAQh4xHcvXuXiqLw69evXFtb4/n5OePxOBcXF/np0yeS5JMnT1T9p6eneXBwoN4p2QgA4PDwsOo9WF9fpyiKqr6PHj1iLBbj7du3VQnELipycnJSjCRJYktLC3t6eri/v0+SHB8fp81mY2FhYYr9xRj19fUkSYvFkp2ATqej3+9nW1sbATA/P58GgyFpbWxskCStVmsKJggCAdDr9bKrq+vnLRdFBgIBvnz5MjsBg8FAknz8+DGLiop4dnamWvp08vz5cwIgSb558yaRZGFhgaurq9cbRIeHh+ju7kZ9fX2SvrOzE8XFxXA6nZBlOaEPBAJwOp1pYy0vL2N0dBSSJKUOJFEU01YAAO12+5V3v7e3lxjHlytQVVVFkjSbzdcbxVNTUzg6OkJZWRm0Wq1qBQ4PD+F2uxGJRNLG2d3dxdHREZqbm/Hhw4erVwAArVZr1t3Pz88n7epyBQBwdnaWKysrmSsQDAaxvb0Nj8eT0K2srKC9vR2SJAEABgcHYTKZ8OzZM/j9fgDA5uZmyo53dnaSdMvLy5iZmUm9B6Io/nORUaahAoCrq6skyZqaGlWbvLy8FJ3JZCJJlpeXJ2OXCVxeBoOB79+/pyzL/P79O6PRKEkyGAwyEAjw4OCAnZ2dGUkDYFNTE0mmDq5sBPr6+rLegY8fP1Kv17O3t5d37txJG+fBgweMRCLMzc29HgGj0UhZllWTx+Nx9vf3U6fTcW9vjycnJ+zv7+etW7cSMfR6Pb1eLxcXF1NzaDSajAT+vxfhcJgTExOJsVtdXU2S7OvrS3pnPH36lMFgkF++fKHD4eDr16/p8Xjo9/tpNBpTY+MKEo/HQRKhUAiyLEOWZRwfHwMAotFowk5RFIyMjMBoNMLlcqG6uhpmsxlbW1tobGyEz+dLiZ31m/BXRJZl2O32K9n+lj8mmUQjiuLfZPajCIfDmJubw+npKYCf34iCIMDlciEUCv0yAUGj0URjsdiNHMVV5I8fgQDgHID03/PFFvktz/8C/6xwoasnBYAAAAAASUVORK5CYII=' >
			XRay pour SPIP
			</a>
		</div>
		</a>
	<hr class="apc">
</div>

<?php
// Les dossiers de squelettes déclarés dans le paquet.xml comme 'public' ne sont pas accessibles dans le privé
// Pour bénéficier des liens ici, il faut les ajouter dans la $GLOBALS['dossier_squelettes']
// echo "<h2>GLOBALS['dossier_squelettes'] : <pre>". print_r($GLOBALS['dossier_squelettes'],1)."</pre></h2>";
// echo "<h2>_chemin</h2> <pre>".print_r(_chemin(),1)."</p>";
?>


<?php
// Display main Menu
echo <<<EOB
	<ol class=menu>
	<li><a href="$MY_SELF&SH={$MYREQUEST['SH']}">Refresh Data</a></li>
EOB;
echo menu_entry(OB_HOST_STATS, 'View Host Stats'), menu_entry(OB_USER_CACHE, 'User Cache Entries'), menu_entry(OB_VERSION_CHECK, 'Version Check');

if (plugin_est_actif('cachelab'))
	echo menu_entry(OB_CACHELAB, 'CacheLab');

	echo <<<EOB
		<li><a class="aright" href="$MY_SELF&CC=1" onClick="javascript:return confirm('Are you sure?');"
			title="Vider le cache APC user">Vider APC</a>
		</li>
		<li><a class="aright" href="$MY_SELF&PP=1" 
				onClick="javascript:return confirm('Êtes-vous certain de vouloir vider le cache APC user et le dossier skel/ des squelettes compilés ?');"
				title="Vider le cache APC user ET effacer les caches de compilation des squelettes ?">
				Purger SPIP</a>
		</li>
	</ol>
EOB;


// CONTENT
echo <<<EOB
	<div class=content>
EOB;

// MAIN SWITCH STATEMENT

switch ($MYREQUEST['OB']) {
	// -----------------------------------------------
	// Host Stats
	// -----------------------------------------------
	case OB_HOST_STATS:
		echo "Hop OB_HOST_STATS demandé<br>";
		include_once ("inc/xray_graphics.php");
		$mem_size         = $mem['num_seg'] * $mem['seg_size'];
		$mem_avail        = $mem['avail_mem'];
		$mem_used         = $mem_size - $mem_avail;
		$seg_size         = bsize($mem['seg_size']);
		$req_rate_user    = sprintf("%.2f", $cache['num_hits'] ? (($cache['num_hits'] + $cache['num_misses']) / ($time - $cache['start_time'])) : 0);
		$hit_rate_user    = sprintf("%.2f", $cache['num_hits'] ? (($cache['num_hits']) / ($time - $cache['start_time'])) : 0);
		$miss_rate_user   = sprintf("%.2f", $cache['num_misses'] ? (($cache['num_misses']) / ($time - $cache['start_time'])) : 0);
		$insert_rate_user = sprintf("%.2f", $cache['num_inserts'] ? (($cache['num_inserts']) / ($time - $cache['start_time'])) : 0);
		$apcversion       = phpversion('apcu');
		$phpversion       = phpversion();
		$number_vars      = $cache['num_entries'];
		$size_vars        = bsize($cache['mem_size']);
		$i                = 0;
		$_namespace       = _CACHE_NAMESPACE;

		$meta_derniere_modif = lire_meta('derniere_modif');

		echo "<div class='info div1'><h2>Mémoization SPIP - Le ".date(JOLI_DATE_FORMAT,time())."</h2>
			<table cellspacing=0><tbody>
			<tr class=tr-0><td class=td-0>Config</td><td><pre>".preg_replace('/(^Array\s*\(|^\s*|^\)$)/im', '', print_r(unserialize($GLOBALS['meta']['memoization']),1))."</pre></td></tr>
			<tr class=tr-0><td class=td-0>_CACHE_NAMESPACE</td><td>"._CACHE_NAMESPACE."</td></tr>
			<tr class=tr-0><td class=td-0>_CACHE_KEY</td><td>"._CACHE_KEY."</td></tr>
			<tr class=tr-0><td class=td-0 title='meta SPIP : derniere_modif'>Dernière invalidation</td><td>".date(JOLI_DATE_FORMAT, $meta_derniere_modif)."</td></tr> 
			<tr class=tr-0><td class=td-0 title='meta spip'>Invalidation de '".XRAY_OBJET_SPECIAL."'</td><td>".date(JOLI_DATE_FORMAT, lire_meta('derniere_modif_'.XRAY_OBJET_SPECIAL))."</td></tr> 
			<tr class=tr-0><td class=td-0 title='meta SPIP : cache_mark'>Dernière purge</td><td>".date(JOLI_DATE_FORMAT, $GLOBALS['meta']['cache_mark'])."</td></tr> ";

		$stats = xray_stats($cache);
		echo xray_stats_print($stats, 'generaux', 'Valides '.XRAY_LABEL_STATS_SPECIALES_EXCLUES);
		echo xray_stats_print($stats, 'speciaux', '+ Valides '.XRAY_LABEL_STATS_SPECIALES);
		echo xray_stats_print($stats, 'invalides', '+ Invalidés par SPIP');
		echo xray_stats_print($stats, 'existent', '= Total caches APC OK');
		echo xray_stats_print($stats, 'fantomes', '+ Caches périmés par APC');
		$nb_cache = count($cache['cache_list']);
		echo "<tr class=tr-0>
			<td class=td-0><b>= Nb total caches APC</b></td><td>$nb_cache</td>
			</tr>";

		echo "</table></div>";

		echo <<< EOB
		<div class="info div1"><h2>General Cache Information</h2>
		<table cellspacing=0><tbody>
		<tr class=tr-0><td class=td-0>APCu Version</td><td>$apcversion</td></tr>
		<tr class=tr-1><td class=td-0>PHP Version</td><td>$phpversion</td></tr>
EOB;

		if (!empty($_SERVER['SERVER_NAME']))
			echo "<tr class=tr-0><td class=td-0>APCu Host</td><td>{$_SERVER['SERVER_NAME']} $host</td></tr>\n";
		if (!empty($_SERVER['SERVER_SOFTWARE']))
			echo "<tr class=tr-1><td class=td-0>Server Software</td><td>{$_SERVER['SERVER_SOFTWARE']}</td></tr>\n";
		
		echo <<<EOB
		<tr class=tr-0><td class=td-0>Shared Memory</td><td>{$mem['num_seg']} Segment(s) with $seg_size
    <br/> ({$cache['memory_type']} memory)
    </td></tr>
EOB;
		echo '<tr class=tr-1><td class=td-0>Start Time</td><td>', date(DATE_FORMAT, $cache['start_time']), '</td></tr>';
		echo '<tr class=tr-0><td class=td-0>Uptime</td><td>', xray_duration($cache['start_time']), '</td></tr>';
		echo <<<EOB
		</tbody></table>
		</div>

		<div class="info div1"><h2>Cache Information</h2>
		<table cellspacing=0>
		<tbody>
    		<tr class=tr-0><td class=td-0>Cached Variables</td><td>$number_vars ($size_vars)</td></tr>
			<tr class=tr-1><td class=td-0>Hits</td><td>{$cache['num_hits']}</td></tr>
			<tr class=tr-0><td class=td-0>Misses</td><td>{$cache['num_misses']}</td></tr>
			<tr class=tr-1><td class=td-0>Request Rate (hits, misses)</td><td>$req_rate_user cache requests/second</td></tr>
			<tr class=tr-0><td class=td-0>Hit Rate</td><td>$hit_rate_user cache requests/second</td></tr>
			<tr class=tr-1><td class=td-0>Miss Rate</td><td>$miss_rate_user cache requests/second</td></tr>
			<tr class=tr-0><td class=td-0>Insert Rate</td><td>$insert_rate_user cache requests/second</td></tr>
			<tr class=tr-1><td class=td-0>Cache full count</td><td>{$cache['expunges']}</td></tr>
		</tbody>
		</table>
		</div>

		<div class="info div2"><h2>Runtime Settings</h2><table cellspacing=0><tbody>
EOB;
		
		$j = 0;
		foreach (ini_get_all('apcu') as $k => $v) {
			echo "<tr class=tr-$j><td class=td-0>", $k, "</td><td>", str_replace(',', ',<br />', $v['local_value']), "</td></tr>\n";
			$j = 1 - $j;
		}
		
		if ($mem['num_seg'] > 1 || $mem['num_seg'] == 1 && count($mem['block_lists'][0]) > 1)
			$mem_note = "Memory Usage<br /><font size=-2>(multiple slices indicate fragments)</font>";
		else
			$mem_note = "Memory Usage";
		
		echo <<< EOB
		</tbody></table>
		</div>

		<div class="graph div3"><h2>Host Status Diagrams</h2>
		<table cellspacing=0><tbody>
EOB;
		$size = 'width=' . (GRAPH_SIZE * 2 / 3) . ' height=' . (GRAPH_SIZE / 2);
		echo <<<EOB
		<tr>
		<td class=td-0>$mem_note</td>
		<td class=td-1>Hits &amp; Misses</td>
		</tr>
EOB;
		
		echo graphics_avail() ?
			'<tr>'
			. "<td class=td-0><img alt='IMG 1' $size src='{$IMG_BASE}&IMG=1&$time'></td>"
			. "<td class=td-1><img alt='IMG 2' $size src='{$IMG_BASE}&IMG=2&$time'></td>
			</tr>\n"
		:
			"", '<tr>', '<td class=td-0><span class="green box">&nbsp;</span>Free: ', bsize($mem_avail) . sprintf(" (%.1f%%)", $mem_avail * 100 / $mem_size), "</td>\n", '<td class=td-1><span class="green box">&nbsp;</span>Hits: ', $cache['num_hits'] . @sprintf(" (%.1f%%)", $cache['num_hits'] * 100 / ($cache['num_hits'] + $cache['num_misses'])), "</td>\n", '</tr>', '<tr>', '<td class=td-0><span class="red box">&nbsp;</span>Used: ', bsize($mem_used) . sprintf(" (%.1f%%)", $mem_used * 100 / $mem_size), "</td>\n", '<td class=td-1><span class="red box">&nbsp;</span>Misses: ', $cache['num_misses'] . @sprintf(" (%.1f%%)", $cache['num_misses'] * 100 / ($cache['num_hits'] + $cache['num_misses'])), "</td>\n";
		echo <<< EOB
		</tr>
		</tbody></table>

		<br/>
		<h2>Detailed Memory Usage and Fragmentation</h2>
		<table cellspacing=0><tbody>
		<tr>
		<td class=td-0 colspan=2><br/>
EOB;
		
		// Fragementation: (freeseg - 1) / total_seg
		$nseg = $freeseg = $fragsize = $freetotal = 0;
		for ($i = 0; $i < $mem['num_seg']; $i++) {
			$ptr = 0;
			foreach ($mem['block_lists'][$i] as $block) {
				if ($block['offset'] != $ptr) {
					++$nseg;
				}
				$ptr = $block['offset'] + $block['size'];
				/* Only consider blocks <5M for the fragmentation % */
				if ($block['size'] < (5 * 1024 * 1024))
					$fragsize += $block['size'];
				$freetotal += $block['size'];
			}
			$freeseg += count($mem['block_lists'][$i]);
		}
		
		if ($freeseg > 1) {
			$frag = sprintf("%.2f%% (%s out of %s in %d fragments)", ($fragsize / $freetotal) * 100, bsize($fragsize), bsize($freetotal), $freeseg);
		} else {
			$frag = "0%";
		}
		
		if (graphics_avail()) {
			$size = 'width=' . (2 * GRAPH_SIZE + 150) . ' height=' . (GRAPH_SIZE + 10);
			echo <<<EOB
			<img alt="IMG 3" $size src="{$IMG_BASE}&IMG=3&$time">
EOB;
		}
		echo <<<EOB
		</br>Fragmentation: $frag
		</td>
		</tr>
EOB;
		if (isset($mem['adist'])) {
			foreach ($mem['adist'] as $i => $v) {
				$cur = pow(2, $i);
				$nxt = pow(2, $i + 1) - 1;
				if ($i == 0)
					$range = "1";
				else
					$range = "$cur - $nxt";
				echo "<tr><th align=right>$range</th><td align=right>$v</td></tr>\n";
			}
		}
		echo <<<EOB
		</tbody></table>
		</div>
EOB;
		
		break;

	// -----------------------------------------------
	// User Cache Entries
	// -----------------------------------------------
	case OB_USER_CACHE:
		include ('xray_browser.php');
		break;
	
	// -----------------------------------------------
	// Version check
	// -----------------------------------------------
	case OB_VERSION_CHECK:
		echo <<<EOB
		<div class="info"><h2>APCu Version Information</h2>
		<table cellspacing=0><tbody>
		<tr>
		<th></th>
		</tr>
EOB;
		if (defined('PROXY')) {
			$ctxt = stream_context_create(array(
				'http' => array(
					'proxy' => PROXY,
					'request_fulluri' => True
				)
			));
			$rss  = @file_get_contents("http://pecl.php.net/feeds/pkg_apcu.rss", False, $ctxt);
		} else {
			$rss = @file_get_contents("http://pecl.php.net/feeds/pkg_apcu.rss");
		}
		if (!$rss) {
			echo '<tr class="td-last center"><td>Unable to fetch version information.</td></tr>';
		} else {
			$apcversion = phpversion('apcu');
			
			preg_match('!<title>APCu ([0-9.]+)</title>!', $rss, $match);
			echo '<tr class="tr-0 center"><td>';
			if (version_compare($apcversion, $match[1], '>=')) {
				echo '<div class="ok">You are running the latest version of APCu (' . $apcversion . ')</div>';
				$i = 3;
			} else {
				echo '<div class="failed">You are running an older version of APCu (' . $apcversion . '),
				newer version ' . $match[1] . ' is available at <a href="http://pecl.php.net/package/APCu/' . $match[1] . '">
				http://pecl.php.net/package/APCu/' . $match[1] . '</a>
				</div>';
				$i = -1;
			}
			echo '</td></tr>';
			echo '<tr class="tr-0"><td><h3>Change Log:</h3><br/>';
			
			preg_match_all('!<(title|description)>([^<]+)</\\1>!', $rss, $match);
			$changelog = $match[2];
			
			for ($j = 2; $j + 1 < count($changelog); $j += 2) {
				$v = $changelog[$j];
				if ($i < 0 && version_compare($apcversion, $ver, '>=')) {
					break;
				} else if (!$i--) {
					break;
				}
				list($unused, $ver) = $v;
				$changes = $changelog[$j + 1];
				echo "<b><a href=\"http://pecl.php.net/package/APCu/$ver\">" . htmlspecialchars($v, ENT_QUOTES, 'UTF-8') . "</a></b><br><blockquote>";
				echo nl2br(htmlspecialchars($changes, ENT_QUOTES, 'UTF-8')) . "</blockquote>";
			}
			echo '</td></tr>';
		}
		echo <<< EOB
		</tbody></table>
		</div>
EOB;
		break;

	case OB_CACHELAB :
		include_spip('inc/cachelab');
		include('cachelab_diag.php');
		break;
}
?>

	</div>
<!--
Based on APCGUI By R.Becker\n$VERSION
-->

</body>
</html>
