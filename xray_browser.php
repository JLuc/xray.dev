<?php

$cols = 7;

echo '<form>
		<input type="hidden" name="OB" value="'.$MYREQUEST['OB'].'">
		<input type="hidden" name="exec" value="'.$MYREQUEST['exec'].'">
		<input type="hidden" name="S_KEY" value="'.$MYREQUEST['S_KEY'].'">
		<input type="hidden" name="TYPELISTE" value="'.$MYREQUEST['TYPELISTE'].'">
		<p style="margin-bottom:10px">
			<b style="margin-bottom:5px">Lister :</b>
			<span style="margin-left: 1em; "></span>
			<select name=COUNT onChange="form.submit()" dir="rtl">
				<option value=10 ', $MYREQUEST['COUNT'] == '10' ? ' selected' : '', ' style="text-align: right">10</option>
				<option value=20 ', $MYREQUEST['COUNT'] == '20' ? ' selected' : '', '>20</option>
				<option value=50 ', $MYREQUEST['COUNT'] == '50' ? ' selected' : '', '> 50</option>
				<option value=100', $MYREQUEST['COUNT'] == '100' ? ' selected' : '', '>100</option>
				<option value=150', $MYREQUEST['COUNT'] == '150' ? ' selected' : '', '>150</option>
				<option value=250', $MYREQUEST['COUNT'] == '250' ? ' selected' : '', '>250</option>
				<option value=500', $MYREQUEST['COUNT'] == '500' ? ' selected' : '', '>500</option>
				<option value=0  ', $MYREQUEST['COUNT'] == '0' ? ' selected' : '', '>Tous</option>
			</select>&nbsp;&nbsp;';

if ($MYREQUEST['TYPELISTE']=='squelettes') {
	echo '<a href="'.parametre_url($MY_SELF, 'TYPELISTE', 'caches').'">Caches</a> 
			| <b>Squelettes</b>';
}
else {
	echo '<b>Caches</b> 
			| <a href="'.parametre_url($MY_SELF, 'TYPELISTE', 'squelettes').'">Squelettes</a>';
};
echo '
			<span style="margin-left: 1em; "></span>
			<b>Afficher :</b> 
			<select name=EXTRA  onChange="form.submit()">
				<option value="" ', $MYREQUEST['EXTRA'] == '' ? ' selected' : '', '></option> 
				<option value=CONTEXTE ', $MYREQUEST['EXTRA'] == 'CONTEXTE' ? ' selected' : '', '>Contexte</option>
				<option value=CONTEXTES_SPECIAUX ', $MYREQUEST['EXTRA'] == 'CONTEXTES_SPECIAUX' ? ' selected' : '', '>Contextes spécifiques</option>
				<option value=HTML_COURT ', $MYREQUEST['EXTRA'] == 'HTML_COURT' ? ' selected' : '', '>HTML (...)</option>
				<option value=INFO_AUTEUR ', $MYREQUEST['EXTRA'] == 'INFO_AUTEUR' ? ' selected' : '', '>Infos auteur</option>
				<option value=INFO_OBJET_SPECIAL ', $MYREQUEST['EXTRA'] == 'INFO_OBJET_SPECIAL' ? ' selected' : '', '>Infos '.XRAY_OBJET_SPECIAL.'</option>
				<option value=INVALIDEURS ', $MYREQUEST['EXTRA'] == 'INVALIDEURS' ? ' selected' : '', '>Invalideurs</option>
				<option value=INVALIDEURS_SPECIAUX ', $MYREQUEST['EXTRA'] == 'INVALIDEURS_SPECIAUX' ? ' selected' : '', '>Invalideurs spécifiques</option>
				<option value=INCLUSIONS ', $MYREQUEST['EXTRA'] == 'INCLUSIONS' ? ' selected' : '', '>&lt;INCLURE&gt;</option>
				<option value=MACROSESSIONS ', $MYREQUEST['EXTRA'] == 'MACROSESSIONS' ? ' selected' : '', '>#_SESSION</option>
				<option value=MACROAUTORISER ', $MYREQUEST['EXTRA'] == 'MACROAUTORISER' ? ' selected' : '', '>#_AUTORISER_SI</option>
			</select>
			</p>';

echo '<p><b>Filtrer caches : </b> 
		<span style="margin-left: 1em; "></span>
		Types
		<select name=TYPECACHE  onChange="form.submit()">
			<option value=ALL', $MYREQUEST['TYPECACHE'] == 'ALL' ? ' selected' : '', '>Tous</option>
			<option value=NON_SESSIONS', $MYREQUEST['TYPECACHE'] == 'NON_SESSIONS' ? ' selected' : '', '>Non sessionnés</option>
			<option value=SESSIONS', $MYREQUEST['TYPECACHE'] == 'SESSIONS' ? ' selected' : '', '>Sessionnés</option>
			<option value=SESSIONS_AUTH', $MYREQUEST['TYPECACHE'] == 'SESSIONS_AUTH' ? ' selected' : '', '>Sessionnés identifiés</option>
			<option value=SESSIONS_NONAUTH', $MYREQUEST['TYPECACHE'] == 'SESSIONS_NONAUTH' ? ' selected' : '', '>Sessionnés non identifiés</option>
			<option value=SESSIONS_TALON', $MYREQUEST['TYPECACHE'] == 'SESSIONS_TALON' ? ' selected' : '', '>Talons de session</option>
			<option value=FORMULAIRES', $MYREQUEST['TYPECACHE'] == 'FORMULAIRES' ? ' selected' : '', '>Formulaires</option>
		</select>
		<span style="margin-left: 1em; "></span>
		&nbsp;&nbsp;&nbsp;
		<span title="REGEXP">avec</span> <input name=SEARCH value="', $MYREQUEST['SEARCH'], '" type=text size=25/>
		<b>dans</b>
		<select name=WHERE onChange="form.submit()">
			<option value="" ', $MYREQUEST['WHERE'] == '' ? ' selected' : '', '>Noms des caches</option>
			<option value="ALL" ', $MYREQUEST['WHERE'] == 'ALL' ? ' selected' : '', '>Tout le contenu</option>
			<option value="HTML" ', $MYREQUEST['WHERE'] == 'HTML' ? ' selected' : '', '>HTML</option>
			<option value="META" ', $MYREQUEST['WHERE'] == 'META' ? ' selected' : '', '>Métadonnées</option>
			<option value="CONTEXTE" ', $MYREQUEST['WHERE'] == 'CONTEXTE' ? ' selected' : '', '>Contexte</option>
		</select>
		&nbsp;&nbsp;&nbsp;
		<input type=submit id="ListSubmit" name="ListSubmit" value="List">';

if (plugin_est_actif('cachelab'))
	echo '<input type=submit id="DelSubmit"  name="DelSubmit" value="X" style="color:red">';

echo '</p>
		</form>
		';

if (isset($MYREQUEST['SEARCH'])) {
	// Don't use preg_quote because we want the user to be able to specify a
	// regular expression subpattern.
	// Detection of a potential preg error :
	if (@preg_match('/' . antislash($MYREQUEST['SEARCH']) . '/i', null) === false) {
		echo '<div class="error">Error: search expression is not a valid regexp (it needs escaping parentheses etc)</div>';
		$MYREQUEST['SEARCH'] = preg_quote($MYREQUEST['SEARCH'],'/');
		echo "<div class='warning'>
						Warning : search expression has been preg_quoted :
							<xmp>{$MYREQUEST['SEARCH']}</xmp>
					</div>";
	}
	$MYREQUEST['SEARCH'] = '~'.$MYREQUEST['SEARCH'].'~i';
}
echo '<div class="info">
				<table cellspacing=0>
					<tbody><tr>';
if ($MYREQUEST['TYPELISTE']=='squelettes')
	echo '<th align="left">', sortheader('S', 'Squelettes').'</th><th></th>';
else {
	echo '<th align="left">Caches - ', sortheader('S', 'tri par Squelette').'</th>',
	'<th>', sortheader('H', 'Hits'), '</th>',
	'<th>', sortheader('Z', 'Size'), '</th>',
	'<th>', sortheader('C', 'Created at'), '</th>',
	'<th>', sortheader('T', 'Timeout'), '</th>',
	'<th>Del</th>
				</tr>';
};

// FIXME : il vaudrait mieux trier aprés avoir filtré

// builds list with alpha numeric sortable keys
//
$list = array();

foreach ($cache['cache_list'] as $i => $entry) {
	switch ($MYREQUEST['S_KEY']) {
		case 'A':
			$k = sprintf('%015d-', $entry['access_time']);
			break;
		case 'H':
			$k = sprintf('%015d-', $entry['num_hits']);
			break;
		case 'Z':
			$k = sprintf('%015d-', $entry['mem_size']);
			break;
		case 'C':
			$k = sprintf('%015d-', $entry['creation_time']);
			break;
		case 'T':
			$k = sprintf('%015d-', $entry['ttl']);
			break;
		case 'S':
			// tri par squelette : on supprime le préfixe et le md5 au début
			// et alors on peut trier
			$k = cache_get_squelette($entry['info']);
			break;
	}
	$list[$k . $entry['info']] = $entry;
}

if ($list) {
	// sort list
	//
	switch ($MYREQUEST['SORT']) {
		case "A":
			ksort($list);
			break;
		case "D":
			krsort($list);
			break;
		default:
			echo "...ah ben non pas de tri.";
			break;
	}

	$TYPECACHE = (isset($MYREQUEST['TYPECACHE']) ? $MYREQUEST['TYPECACHE'] : 'ALL');
	$also_required='';
	$also_required_bool = true;
	switch ($TYPECACHE) {
		case 'ALL':
			$pattern_typecache = '';
			break;
		case 'NON_SESSIONS':
			$pattern_typecache = XRAY_PATTERN_NON_SESSION;
			$also_required = 'cache_est_talon';
			$also_required_bool = false;
			break;
		case 'SESSIONS':
			$pattern_typecache = XRAY_PATTERN_SESSION;
			break;
		case 'SESSIONS_AUTH':
			$pattern_typecache = XRAY_PATTERN_SESSION_AUTH;
			break;
		case 'SESSIONS_NONAUTH':
			$pattern_typecache = XRAY_PATTERN_SESSION_ANON;
			break;
		case 'SESSIONS_TALON':
			$pattern_typecache = XRAY_PATTERN_NON_SESSION;
			$also_required = 'cache_est_talon';
			break;
		case 'FORMULAIRES':
			$pattern_typecache = '~formulaires/~i';
			break;
	}

	$liste_squelettes = array();

	// output list
	$i = 0;
	foreach ($list as $k => $entry) {
		$data=$searched=null;
		$data_success = false;
		// désormais on cherche toujours data
		$searched = $data = get_apc_data($entry['info'], $data_success);

		if ($MYREQUEST['SEARCH'] and $MYREQUEST['WHERE']) {
			switch ($MYREQUEST['WHERE']) {
				case 'ALL' :
					break;
				case 'HTML' :
					if (is_array($searched)) // !textwheel {
						$searched = $data['texte'];
					break;
				case 'META' :
					if (is_array($searched)) // !textwheel
						unset($searched['texte']);
					break;
				case 'CONTEXTE' :
					if (is_array($searched)
						and isset($searched['contexte'])) // !textwheel
						$searched = $searched['contexte'];
					break;
				default :
					die("Mauvaise valeur pour where : " . $MYREQUEST['WHERE']);
			}
		};

		if ((!isset($pattern_typecache) or !$pattern_typecache or preg_match($pattern_typecache, $entry['info']))
			and (!$MYREQUEST['SEARCH']
				or (!$MYREQUEST['WHERE']
					and preg_match($MYREQUEST['SEARCH'], $entry['info']))
				or ($MYREQUEST['WHERE']
					and preg_match($MYREQUEST['SEARCH'].'m', print_r($searched,1))))
			and (!$also_required
				or ($also_required($entry['info'], $data)== $also_required_bool))
		) {
			$descriptif = "%s caches listés, sur un total de %s";
			if (isset($_REQUEST['DelSubmit']) and ($_REQUEST['DelSubmit']=='X')) {
				cachelab_cibler('del', array ('chemin'=>$entry['info']));
				$i++;
				$descriptif = "%s caches effacés sur un total de %s caches";
				continue;
			}
			elseif ($MYREQUEST['TYPELISTE']=='squelettes') {
				$descriptif = "%s squelettes listés, pour un total de %s caches";
				$joli = array();
				if (!is_array($data)) {	// textwheel etc
					continue;
				}
				$radical = cache_get_squelette($entry['info']);

				if (isset($data['source']))
					$source = $data['source'];
				else
					// talons (sans 'source')
					// Le radical est relatif aux CHEMINs SPIP
					// Pour le lien il faut l'adresse systeme
					$source = find_in_path($radical.'.html');

				if (array_key_exists($radical, $liste_squelettes)) {	// déjà listé
					$liste_squelettes[$radical]['nb']++;
					if (!$liste_squelettes[$radical]['source'])	// au cas où
						$liste_squelettes[$radical]['source'] = $source;
					continue;
				}

				// squelette pas encore listé
				if ($i < $MYREQUEST['COUNT']) {
					$i++;
					$liste_squelettes[$radical] = array('nb'=>1, 'source'=>$source);
				}
				// Même aprés avoir le bon compte à afficher, on continue à scanner les caches
				// pour avoir à la fin le bon nombre de caches par squelettes
				continue;
			} // Fin du cas "on liste les squelettes"

			// Maintenant on liste les caches
			$i++;
			$sh = md5($entry["info"]);

			$displayed_name = htmlentities(strip_tags($entry['info'], ''), ENT_QUOTES, 'UTF-8');
			if (defined('XRAY_NEPASAFFICHER_DEBUTNOMCACHE'))
				$displayed_name = str_replace(XRAY_NEPASAFFICHER_DEBUTNOMCACHE, '', $displayed_name);
			echo '<tr id="key-' . $sh . '" class=tr-', $i % 2, '>',
			"<td class='td-0' style='position: relative'>
								$i) 
								<a href='$MY_SELF&SH={$sh}#key-{$sh}'>
									$displayed_name
								</a>";

			if ($data and cache_est_talon($entry['info'], $data))
				echo "<span style='margin-left:2em' title='Talon des caches sessionnés avec ce squelette et le même contexte'>[talon]</span>";

			$boutons_liens = '';
			if ($p = preg_match(XRAY_PATTERN_SESSION_AUTH, $displayed_name, $match)
				and $MYREQUEST['SEARCH'] != "/{$match[1]}/i") {
				$url_session = parametre_url($MY_SELF, 'SEARCH', $match[1]);
				$boutons_liens .= bouton_session($match[1], $url_session);
			}
			if (is_array($data)
				and isset($data['invalideurs']['session'])) {
				$p = preg_match(XRAY_PATTERN_TALON, $displayed_name, $match);
				$url_mm_talon = '';
				$bouton_mm_talon='[mm talon]';
				if ($p and $match[1] and ($MYREQUEST['SEARCH']!=$match[1])) {
					$url_mm_talon = parametre_url($MY_SELF, 'TYPECACHE', 'ALL');
					$url_mm_talon = parametre_url($url_mm_talon, 'SEARCH', $match[1]);
				}
				else
					$bouton_mm_talon='(! Err get talon !)';
				$boutons_liens .= "<a href='$url_mm_talon' title='Caches du même squelette et avec le même contexte'>$bouton_mm_talon</a>";
			}
			echo '<span style="float: right">'.$boutons_liens.'</span>';

			if ($MYREQUEST['EXTRA'] and ($sh != $MYREQUEST["SH"]) // sinon yaura un zoom après et c'est inutile de répéter ici
				and $data_success) {
				$extra = null;
				$jolif='joli_cache';
				if (is_array($data)) {
					switch ($MYREQUEST['EXTRA']) {
						case 'CONTEXTE':
							$jolif='joli_contexte';
							if (isset($data['contexte']))
								$extra = $data['contexte'];
							else
								$extra = '(non défini)';
							break;

						case 'CONTEXTES_SPECIAUX':
							if (isset($data['contexte'])) {
								$jolif='joli_contexte';
								$extra = $data['contexte'];
								foreach (array(
									         'lang',
									         'date',
									         'date_default',
									         'date_redac',
									         'date_redac_default'
								         ) as $ki)
									unset($extra[$ki]);
							} else
								$extra = '(non défini)';
							break;

						case 'HTML_COURT' :
							$extra = ajuste_longueur_html($data['texte']);
							break;

						case 'INFO_AUTEUR':
							$jolif='joli_contexte';
							if (isset($data['contexte'])) {
								foreach (array(
									         'id_auteur',
									         'email',
									         'nom',
									         'statut',
									         'login'
								         ) as $ki)
									if (isset($data['contexte'][$ki]))
										$extra[$ki] = $extra[$ki] = $data['contexte'][$ki];
							};
							break;

						case 'INFO_OBJET_SPECIAL':
							$jolif='joli_contexte';
							if (isset($data['contexte'])) {
								$ki = 'id_'.XRAY_OBJET_SPECIAL;
								if (isset($data['contexte'][$ki]))
									$extra[$ki] = $extra[$ki] = $data['contexte'][$ki];
							};
							break;
						case 'INVALIDEURS':
							if (isset ($data['invalideurs']))
								$extra = $data['invalideurs'];
							break;
						case 'INVALIDEURS_SPECIAUX':
							if (isset ($data['invalideurs'])) {
								$extra = $data['invalideurs'];
								foreach (array(
									         'cache',
									         'session'
								         ) as $ki)
									unset($extra[$ki]);
							}
							break;
						case 'INCLUSIONS' :
							if (!isset ($data['texte']))
								$extra = '(html non défini)';
							elseif (preg_match_all("/<\?php\s+echo\s+recuperer_fond\s*\(\s*'([a-z0-9_\-\.\/]+)'/", $data['texte'], $matches))
								$extra = $matches[1];
							else
								$extra = '(aucune <INCLUSION>)';
							break;
						case 'MACROSESSIONS' :
							if (!isset ($data['texte']))
								$extra = '(html non défini)';
							elseif (preg_match_all("/\bpipelined_session_get\s*\((['\"a-z0-9\s_\-\.\/,]+)\)/", $data['texte'], $matches))
								$extra = $matches[1];
							else
								$extra = '(aucune balise #_SESSION ou #_SESSION_SI)';
							break;
						case 'MACROAUTORISER' :
							if (!isset ($data['texte']))
								$extra = '(html non défini)';
							elseif (preg_match_all("/if\s+\(autoriser\s*\((.+)\)\s*\)\s*{\s*\?>/", $data['texte'], $matches))
								$extra = $matches[1];
							// $extra = $matches;
							else
								$extra = '(aucune balise #_AUTORISER_SI)';
							break;
					}
				}
				$extra = $jolif($extra);

				if ($extra)
					echo "<br>".$extra."<br>";
				else
					echo "<br>(rien)</br>";
			} // fin affichage Extra

			echo '</td>
					<td class="td-n center">', $entry['num_hits'], '</td>
					<td class="td-n right">', $entry['mem_size'], '</td>
					
					<td class="td-n center">', date(DATE_FORMAT, $entry['creation_time']), '</td>';

			if ($entry['ttl'])
				echo '<td class="td-n center">' . $entry['ttl'] . ' seconds</td>';
			else
				echo '<td class="td-n center">None</td>';

			echo '<td class="td-last center">';
			echo '<a href="', $MY_SELF, '&DU=', urlencode($entry['info']), '" style="color:red">X</a>';
			echo '</td></tr>';

			if ($sh == $MYREQUEST["SH"]) { // Le ZOOM sur une entrée
				echo '<tr>';
				echo '<td colspan="7">';

				if (!isset($_GET['ZOOM']) or ($_GET['ZOOM'] != 'TEXTELONG')) {
					$url      = parametre_url($self_pour_lien, 'ZOOM', 'TEXTELONG') . "#key-$sh";
					$menuzoom = "<a href='$url' class='menuzoom'>Voir tout le texte</a> ";
					if (is_array($data) and isset($data['texte']))
						$data['texte'] = ajuste_longueur_html($data['texte']);
				} else {
					$url      = parametre_url($self_pour_lien, 'ZOOM', 'TEXTECOURT') . "#key-$sh";
					$menuzoom = "<a href='$url' class='menuzoom'>Voir texte abbrégé</a> ";
				}
				$url = parametre_url($self_pour_lien, 'SH', '') . "#key-$sh";
				$menuzoom .= "<a href='$url' class='menuzoom'>Replier</a>";

				if ($data_success) {
					echo "<p>$menuzoom</p>";
					echo joli_cache($data);
				} else
					echo '! Échec ou pas un cache spip ( '.explique_echec($entry['info']).' )';
				echo '</td>';
				echo '</tr>';
			} // fin du zoom SH
			if ($i == $MYREQUEST['COUNT'])
				break;
		} // fin du filtrage
	} // fin du foreach

} else { // En l'absence de tout cache
	echo '<tr class=tr-0><td class="center" colspan=', $cols, '><i>No data</i></td></tr>';
}
// Si on liste les squelettes, on a récolté et compté mais encore rien affiché
if ($MYREQUEST['TYPELISTE']=='squelettes') {
	$i=1;
	foreach ($liste_squelettes as $radical => $s) {
		$squel_caches = parametre_url(
			parametre_url($self_pour_lien, 'SEARCH', $radical),
			'TYPELISTE', 'caches');
		// Les dossiers de squelettes déclarés comme public dans paquet.xml
		// ne sont pas utilisés par find_in_path dans le privé

		echo "<tr class='tr-".($i % 2)."'>
					<td>$i) ";
		if (!$s['source'])
			echo $radical.' <span title="Parfois la forme du squelette n’est pas prévue par CacheLab, mais avez-vous déclaré le chemin de vos dossiers publics de squelettes dans GLOBALS[\'dossier_squelette\'] ?">(échec find_in_path)</span>';
		else
			echo joli_cache(array('source'=>$s['source']));
		echo "</td>
					<td><a href='$squel_caches'>[{$s['nb']} caches]</a></td>
				</tr>";
		$i++;
	}
}
echo "
		</tbody></table>
		</div>";

if (isset ($descriptif))
	printf("<p>$descriptif</p>", $i, count($list));

